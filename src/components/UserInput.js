import {useState } from 'react';

const UserInput = (props) => {

    const textarea1Handler = (e) => {
        props.setTextArea1(e.target.value);
    }

    const textarea2Handler = (e) => {
        props.setTextArea2(e.target.value);
    }

    return(
        <div className="userInput">
            <textarea onChange={textarea1Handler}/>
            <textarea onChange={textarea2Handler}/>
        </div>


    )
}

export default UserInput; 