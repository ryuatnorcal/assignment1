
const UserOutput = (props) => {

    return (
        <div className="userOutput">
            <p>{props.text1}</p>
            <p>{props.text2}</p>
        </div>
    )
}

export default UserOutput; 